#!/bin/bash
# kelvin.wong@foxtel.com.au
# Perform debug actions iQ3 (debug) boxes by box or rack or all boxes.
# Version 3.1

# Perform tasks on the boxes that fall inbetween the following range 
readonly TASK=$1
readonly RACK_ARRAY_FIRST=$2
readonly RACK_ARRAY_LAST=$3
readonly BOX_ARRAY_FIRST=$4
readonly BOX_ARRAY_LAST=$5
readonly HEAD_NET="10.116."
#readonly HEAD_NET="192.168."
readonly BOX_ARRAY_LIST="list.$(date +%FT%T).tmp"

function help {
	echo $0 is for performing debug tasks on iQ3 debug STB
	echo "  " with an IP Address from $BOX_ARRAY_SUBNET[First_Box] to $BOX_ARRAY_SUBNET[Last_Box]
	
	echo "  " Syntax
	echo "    " $0 [Option] [First_Rack] [Last_Rack] [First_Box] [Last_Box]
	
	./debug_list.sh
	exit
}

function runTask {
	local taskID=$1

	echo $0 is now executing on $HEAD_NET$RACK_ARRAY_FIRST.$BOX_ARRAY_FIRST to $HEAD_NET$RACK_ARRAY_LAST.$BOX_ARRAY_LAST
	
	# Expand the ip address range
	for rack in $(seq $RACK_ARRAY_FIRST $RACK_ARRAY_LAST)
	do 
		subnet="$HEAD_NET$rack."
		for slot in $(seq $BOX_ARRAY_FIRST $BOX_ARRAY_LAST)
		do
			box="$subnet$slot"
			echo $box >> $BOX_ARRAY_LIST
		done
	done

	./debug_list.sh $TASK $BOX_ARRAY_LIST
	rm $BOX_ARRAY_LIST
}


# Start of Script!
if [ $# -eq 5 ]
then
	runTask 
else
	echo $0 - Unexpected number of arguments: $#
	help
fi
