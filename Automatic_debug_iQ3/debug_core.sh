#!/bin/bash
# kelvin.wong@foxtel.com.au
# Perform debug actions iQ3 (debug) boxes by box or rack or all boxes.
# Version 3.4

# Perform tasks on the boxes that fall inbetween the following range
readonly TASK=$1
readonly BOX=$2

readonly TIMEOUT=60

# Colours
readonly COLOUR_ERROR='-e \033[0;31m Error:\033[0m'

function help {
	echo $0 is for performing debug tasks on iQ3 debug STB
	echo "  " with an IP Address for Box at [IP_Address]

	echo "  " Syntax
	echo "    " $0 [Option] [IP_Address]

	echo "  " [Option] includes:
	echo "    " a, collect and download full logs + archive
	echo "    " d, download the latest log available
	echo "    " f, factory reset box
	echo "    " l, collect and download full logs
	echo "    " r, reboot box
	echo "    " p, ping box
	echo "    " v, get Software version number

	exit
}


# Download the latest log available on box
function downloadLog {
        # base path to server
	local box=$BOX
	local base="http://$box:3210/download/"
        #echo Attempting to download from $base

        # Find the newest log in 192.168.1.31/download/
	# -q 		quiet mode
	# -O -		no output
        local targetLog="$(wget -q -O - $base | grep -o "E733-MAC[^\']*.tar.gz\"" | tail -n1)"

        # Delete last character "
        #release=${releaseDirty::-1} # requires bash 4.2
        targetLog=${targetLog%?}    # compatible with bash 4.1.7

        # Download file
				wget -q -O $box"_"$targetLog $base$targetLog
	# echo curl -o $box"_"$targetLog $base$targetLog
}

# Ignore host key checking when connecting to box via SSH
function sshCommand {
	local box=$1
        local commands=$2  # Commands received
        local sshError=255 # Assume failed

        # Connect to iQ3 via SSH
        # -q                                    # quiet mode
        # -o connectTimeout=10                  # reduce wait time for testing
        # -o UserKnownHostsFile=/dev/null       # discard host key
        # -o StrictHostKeyChecking=no           # ignore host key
        ssh -q -o ConnectTimeout=$TIMEOUT -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$box "$commands" 2>/dev/null

	sshError=$?
	echo $sshError
}

# Collect full logs for box and then download it
function logBox {
	local box=$1

	echo "  "Attempting to collect full logs for box: $box

	local error=$(sshCommand $box "cd /www/htdocs/rdiag/cgi-bin/ && ./collectlogs_full.sh")
	sshErrorCheck $error
}

# Collect full logs + archive for box and then download it
function logArchiveBox {
	local box=$1

	echo "  "Attempting to collect full logs + archive for box: $box

	local error=$(sshCommand $box "cd /www/htdocs/rdiag/cgi-bin/ && ./collectlogs_fullarchived.sh")
	sshErrorCheck $error
}

# FSR box
function fsrBox {
	local box=$1

	echo "  "Attempting to FSR box: $box

	#sshCommand $box './www/htdocs/rdiag/cgi-bin/factory_reset.sh'			# requires bash to be installed or modify for sh
	local error=$(sshCommand $box "echo > /mnt/ffs/reset && /sbin/reboot && exit")	# doesn't require bash and modify system

	sshErrorCheck $error
}

# Check if ssh connected properly
function sshErrorCheck {
	local error=$1

	if [[ $error -ne 0 ]]; then
		echo $COLOUR_ERROR Cannot connect to $box
	elif [[ $error == "OK" ]]; then
		downloadLog $box
	fi
}

# Reboot box
function rebootBox {
	local box=$1
	echo "  "Attempting to reboot box: $box
	local error=$(sshCommand $box "/sbin/reboot")
	sshErrorCheck $error
}

# Get software version from box
function getBoxVersion {
	#local error=$(sshCommand $BOX "cat /www/version.txt")

	commands="cat /www/version.txt"
	version=$(ssh -q -o ConnectTimeout=$TIMEOUT -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$BOX "$commands" 2>/dev/null)
	if [[ $? -ne 0 ]]
	then
		version="error: connection timed out"
	fi
	echo "box,$BOX,version,$version"
}

# Ping IP Address of Box
# Both Debug and Production builds are Pingable!
function pingBox {
	attempts=1
	ping=$(ping -c $attempts $BOX > /dev/null && echo "up" || echo "down")
	echo box,$BOX,network,$ping
}

# Task for box
function runTask {
	# Check for valid taskes
	# Run the task for this box
	case $TASK in
		l) logBox $BOX;;
		a) logArchiveBox $BOX;;
		f) fsrBox $BOX;;
		r) rebootBox $BOX;;
		v) getBoxVersion;;
		d) downloadLog $BOX;;
		p) pingBox;;
		?) echo $COLOUR_ERROR Invalid taskID: $TASK; help;
	esac
	wait
}

# Start of Script!
# Only run script when there is two arguments
# Otherwise goto help menu
if [ $# -eq 2 ]
then
	runTask
else
	help
fi
