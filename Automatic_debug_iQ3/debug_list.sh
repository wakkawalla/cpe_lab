#!/bin/bash
# kelvin.wong@foxtel.com.au
# Perform debug actions iQ3 (debug) boxes by box or rack or all boxes.
# Version 3.0

readonly TASK=$1
# List of IP Addresses
readonly LIST=$2

function help {
	echo $0 is for performing debug tasks on iQ3 debug STB
	echo "  " with an IP Address from list file
	
	echo "  " Syntax
	echo "    " $0 [Option] [List_of_boxes.txt]
	
	./debug_core.sh
	exit
}

function runTask {
	echo -n "Are you sure you want to: $(./debug_core.sh | grep "$TASK,") (y to continue) > "
	read confirmation
	if [[ $confirmation == "y" ]]
	then
		if [[ -f $LIST ]] 
		then 
			echo "  Running..."
			for box in $(cat $LIST) 
			do
				./debug_core.sh $TASK $box &
			done
			wait
		else
			echo $0 - File: $LIST not found
		fi
	else
		echo "  Aborting..."
	fi
}

# Start of Script!
if [ $# -eq 2 ]
then
	runTask
else
	help
fi
