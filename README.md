# Scripts for CPE-Lab #
## Automatic Debug Script for iQ3 Debug ##

### What is this script for? ###
The script is for automating the some of the debugging activities on the STB.
### How do I get set up? ###
1. Install a Linux distribution (Ubuntu 16.04 is recommended)
2. Ensure your STB is running a **Debug** build. Check by visiting [ip address of iQ3 STB]:3210; NB. If you can ping the box, it does not automatically mean it is a debug build!
3. Download the script and start running
### What can I do with this script? ###
You can find out what this script does by running the script with no arguments. Take note of the last paragraph, it shows you the list of options you can execute with this script. For example:

```
#!bash
$ ./debug_auto.sh 
./debug_auto.sh - Unexpected number of arguments: 0
./debug_auto.sh is for performing debug tasks on iQ3 debug STB
   with an IP Address from [First_Box] to [Last_Box]
   Syntax
     ./debug_auto.sh [Option] [First_Rack] [Last_Rack] [First_Box] [Last_Box]
./debug_list.sh is for performing debug tasks on iQ3 debug STB
   with an IP Address from list file
   Syntax
     ./debug_list.sh [Option] [List_of_boxes.txt]
./debug_core.sh is for performing debug tasks on iQ3 debug STB
   with an IP Address for Box at [IP_Address]
   Syntax
     ./debug_core.sh [Option] [IP_Address]
   [Option] includes:
     a, collect and download full logs + archive
     d, download the latest log available
     f, factory reset box
     l, collect and download full logs
     r, reboot box
     p, ping box
     v, get Software version number
```
### How do I choose my box? ###
#### Automatic by Sequence ####
You have a of boxes that are sequence, you can use **debug_auto.sh** to loop through a sequence list for you, the syntax for this script is: 
```
#!bash
./debug_auto.sh [Option]  [First_Rack] [Last_Rack] [First_Box] [Last_Box]
```
So for example, you can get the version number for rack 1 to 3 with 16 boxes each:
```
#!bash
./debug_auto.sh v 1 3 1 16
```
You might experience, connection not found or connection timeout with this automatic sequence because the subnet is incorrect. The script assumes you dont change your subnet often, so it is not exposed on the same level as other arguments. You can change the subnet by opening the script with a text editor and going to line 12. For example in vi:

```
#!bash
vi debug_auto.sh +12
```
it will show you:
```
#!bash
readonly HEAD_NET="10.116."
```
It currently is set to the stable subnet, you can then change that to 192.168. for regular networks by:
```
#!bash
readonly HEAD_NET="192.168."
```
Then save by typing 
```
esc :wq
```
#### Automatic by List ####
So you have a predefined list, that may not follow a sequence of numbers. You can use **debug_list.sh** to loop through a list of your custom predefined list, the syntax for this script is: 
```
#!bash
./debug_auto.sh [Option]  [path/to/ip/address/list]
```
You can find I have already generated a list of [ip addresses](https://bitbucket.org/wakkawalla/cpe-lab/src/0d69fa7cacfe575ffdc95edd4ff37af873e0c456/Automatic_debug_iQ3/ipaddresses/?at=master) for you in this repository. So for example, you can get the full logs (only) for [rack1-7](https://bitbucket.org/wakkawalla/cpe-lab/src/0d69fa7cacfe575ffdc95edd4ff37af873e0c456/Automatic_debug_iQ3/ipaddresses/celab-rack1-7.txt?at=master&fileviewer=file-view-default) in celab by:
```
#!bash
./debug_auto.sh l ipaddresses/celab-rack1-7.txt
```
To make your own custom list, you just need to enter the **ip address** for the box into the text file (one per line). For example box 10 on both rack 21 and rack 30, has an ip address of 10.116.21.10 and 10.116.30.10 (using stable subnet) respectively.
```
10.116.21.10
10.116.30.10
```
#### Manually one by one ####
And finally, you might need to fsr one of the boxes, you can use **debug_core.sh** to just fsr one box at a time. For example you want to fsr your box on your table which has an ip address of 192.168.1.30
```
#!bash
./debug_auto.sh f 192.168.1.30
```
### Dependencies ###
The script is dependant on the core functionality of the script below it. This allows for a modular design, and only change code once in the script to distribute the functionality across all the scripts supported. The following is the hierachy.

```
debug_auto.sh
|debug_list.sh
||debug_core.sh
```
# I need help #
kelvin.wong@foxtel.com.au