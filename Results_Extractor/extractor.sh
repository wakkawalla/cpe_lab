# Author:      Kelvin Wong
# Email:       kelvinwong@outlook.com.au
# Description: Pull specific results out from results file
# Version:     1.1
# Last Change: Allows for sorting based on Test Name

#!/bin/bash

# Output file
file_results=results.csv
rm $file_results

# Command line arguments
#file=$1 # Files to look for
# Get files
for file in $(ls *.txt)
do
	# Preparation
	box=$(grep 'SIGNAL MAP' $file | awk '{print $NF}')
	ipaddress=$(grep 'atedbad version' $file | awk '{print $NF}')
	body=$(egrep 'RUNNING SCRIPT|averageTime' $file | awk '{print $NF}' | tr '\n' ',')

	# Find out if this is a Satellite or Cable box based on the IP address
	if [[ $ipaddress = *"172.22.104.110"* ]]
	then
		box_type="CAB$box"
	elif [[ $ipaddress = *"172.22.104.10"* ]]
	then
		box_type="SAT$box"
	else
		box_type="$file empty"
	fi

	# Print out the test results
	echo "$box_type $body" >> $file_results
done

cat $file_results | sort -k 2 | tr ' ' ','
